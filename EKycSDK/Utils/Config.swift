//
//  Config.swift
//  EKycSDK
//
//  Created by Gia Tu Nguyen on 15/09/2021.
//

import Foundation

let KLP_SERVICE_SERVER = "https://ekyc-dev-internal.kalapa.vn/api"
let KLP_SERVICE_GET_SESSION = "/auth/get-token"
let KLP_SERVICE_CHECK_FRONT = "/kyc/check-front"
let KLP_SERVICE_CHECK_BACK = "/kyc/check-back"
let KLP_SERVICE_GET_DATA = "/data/get"
let KLP_SERVICE_PING = "/kyc/ping"
//let LIVENESS = "/kyc/check-front"
let KLP_SERVICE_LIVENESS_OFFER = "https://ekyc-dev-internal.kalapa.vn/liveness/offer"
let KLP_SOCKET_LISTENER = "https://ekyc-dev-internal.kalapa.vn/socket?token="
let DEV_TUNG_SOCKET_LISTENER = "http://68.183.184.163:8092"

let GET_DATA = "/data/get"
let GET_IMAGE = "/data/image"

// Set this to the machine's address which runs the signaling server. Do not use 'localhost' or '127.0.0.1'
//fileprivate let defaultSignalingServerUrl = URL(string: "ws://192.168.1.132:8080")!

// We use Google's public stun servers. For production apps you should deploy your own stun/turn servers.
fileprivate let defaultIceServers = ["turn:27.71.226.88:64001?transport=tcp"]

struct Config{
    //    let signalingServerUrl: URL
    let webRTCIceServers: [String]
    //    signalingServerUrl: defaultSignalingServerUrl,
    static let `default` = Config(webRTCIceServers: defaultIceServers)
}

let LIVENESS_UNVERIFIED: Int = 0
let LIVENESS_VERIFIED: Int = 1
let LIVENESS_EXPIRED: Int = 2
let LIVENESS_FAILED: Int = 3
let LIVENESS_OTHER_ERROR: Int = 99


func getLivenessMessage(status: Int, input: String) -> String{
    if status == LIVENESS_EXPIRED{
        return "Phiên xác thực hết hạn, vui lòng thử lại";
    }
    switch input {
    case "HoldSteady3Second":
        return "Giữ đầu ngay ngắn, nhìn thẳng trong 3 giây";
    case "TurnLeft":
        return "Quay trái";
    case "TurnRight":
        return "Quay phải";
    case "TurnUp":
        return "Quay lên";
    case "TurnDown":
        return "Quay xuống";
    case "HoldSteady2Second":
        return "Giữ đầu ngay ngắn, nhìn thẳng trong 2 giây";
    case "EyeBlink":
        return "Nháy mắt";
    case "ShakeHead":
        return "Lắc đầu";
    case "NodeHead":
        return "Gật đầu";
    case "Success":
        return "Good!";
    case "Connecting":
        return "Đang kết nối...";
    case "Initializing":
        return "Đang khởi tạo...";
    case "Processing":
        if (status == 1){
            return "Xác thực thành công";
        }
        if (status == 3){
            return "Xác thực thất bại";
        }
        return "Đang xác thực...";
    case "ConnectionFailed":
        return "Kết nối thất bại";
    case "Finished":
        return "Hoàn thành!";
    default:
        return input;
    }
}

func getIconGuider(input: String)-> String{
    
    switch (input) {
    case "HoldSteady3Second":
        return "head_straight"
    case "HoldSteady2Second":
        return "head_straight"
    case "TurnLeft":
        return "head_rotate"
    case "TurnRight":
        return "head_right"
    case "TurnUp":
        return "liveness_head_up"
    case "TurnDown":
        return "head_down"
    case "EyeBlink":
        return "liveness_eye"
    case "ShakeHead":
        return "head_rotate"
    case "NodeHead":
        return "head_down"
    case "Success":
        return "result_success"
    case "Connecting":
        return "head_straight"
    case "Initializing":
        return "head_straight"
    case "ConnectionFailed":
        return "status_bar_closed_default_background"
    default:
        return "head_straight"
    }
}
