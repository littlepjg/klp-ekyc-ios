//
//  Common.swift
//  EKycSDK
//
//  Created by Gia Tu Nguyen on 22/09/2021.
//

import Foundation
import UIKit
import WebRTC

func getCurrentTimeMillis() -> Double{
    NSDate().timeIntervalSince1970 * 1000
}

func getHeighOfRow(input: String, heightPerRow: Int) -> Int{
    let numberOfLines = input.count / 36 + 1
    if(numberOfLines > 2){
        return (numberOfLines * heightPerRow + numberOfLines * 2 )
    }else{
        return (numberOfLines * heightPerRow )
    }
}

func getHeightForView(list: [ElementObj],heightPerRow: Int) -> Int{
    var numberOfLines = 0
    for obj in list{
        let max_line = obj.name.count / 36 > obj.value.count / 36 ? obj.name.count + 1 : obj.value.count / 36 + 1
        numberOfLines += max_line
    }
    return (numberOfLines * heightPerRow + numberOfLines * 2 )
}

func getHeightForView(list: [ElementAntifraudObj],heightPerRow: Int) -> Int{
    var numberOfLines = 0
    for obj in list{
        let max_line = obj.name.split(separator: "\n").count <    obj.value.split(separator: "\n").count ?  obj.value.split(separator: "\n").count :  obj.name.split(separator: "\n").count;
        numberOfLines += max_line
    }
    
    return (numberOfLines * heightPerRow + numberOfLines * 2)
}

func getMatchedByGroup(input: String, regex: NSRegularExpression) -> [String]{
    // return String array
    let inputRange = NSRange(
        input.startIndex..<input.endIndex,
        in: input
    )
    var results:[String] = []
    let matches = regex.matches(in: input, options: [], range: inputRange)
    // For each matched range, extract the capture group
    for match in matches{
        for rangeIndex in 0..<match.numberOfRanges {
            let matchRange = match.range(at: rangeIndex)
            
            // Ignore matching the entire username string
            if matchRange == inputRange { continue }
            
            // Extract the substring matching the capture group
            if let substringRange = Range(matchRange, in: input) {
                let capture = String(input[substringRange])
                results.append(capture)
            }
        }
    }
    
    return results
}

func playVibrate(){
    AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
}

func combineIDCard(frontCard: IDCard, backCard: IDCard) -> IDCard{
    var idCard = frontCard
    do{
        if(backCard.data?.fields != nil){
            try idCard.data?.fields?.features = backCard.data?.fields?.features ?? ""
            try idCard.data?.fields?.doi = backCard.data?.fields?.doi ?? ""
            try  idCard.data?.fields?.poi = backCard.data?.fields?.poi ?? ""
            if backCard.data?.fields?.ethnicity != nil {
                try     idCard.data?.fields?.ethnicity = backCard.data?.fields?.ethnicity ?? ""
            }
            if backCard.data?.fields?.religion != nil {
                try idCard.data?.fields?.religion = backCard.data?.fields?.religion ?? ""
            }
        }
    }catch{
        print("Combine Exception: \(error.localizedDescription)")
    }
    return idCard
    //    let birthday: String
    //    let country: String
    //    let doe: String
    //    let doi: String
    //    let ethnicity: String
    //    let features: String
    //    let gender: String
    //    let home: String
    //    let home_entities: HomeEntities?
    //    let id_number: String
    //    let name: String
    //    let national: String
    //    let poi: String
    //    let religion: String
    //    let resident: String
    //    let resident_entities: ResidentEntities?
    //    let type: String
}

func cropToBounds(image: UIImage, width: Double, height: Double) -> UIImage {
    
    let cgimage = image.cgImage!
    let contextImage: UIImage = UIImage(cgImage: cgimage)
    let contextSize: CGSize = contextImage.size
    var posX: CGFloat = 0.0
    var posY: CGFloat = 0.0
    var cgwidth: CGFloat = CGFloat(width)
    var cgheight: CGFloat = CGFloat(height)
    
    // See what size is longer and create the center off of that
    if contextSize.width > contextSize.height {
        posX = ((contextSize.width - contextSize.height) / 2)
        posY = 0
        cgwidth = contextSize.height
        cgheight = contextSize.height
    } else {
        posX = 0
        posY = ((contextSize.height - contextSize.width) / 2)
        cgwidth = contextSize.width
        cgheight = contextSize.width
    }
    
    let rect: CGRect = CGRect(x: posX, y: posY, width: cgwidth, height: cgheight)
    
    // Create bitmap image from context using the rect
    let imageRef: CGImage = cgimage.cropping(to: rect)!
    
    // Create a new image based on the imageRef and rotate back to the original orientation
    let image: UIImage = UIImage(cgImage: imageRef, scale: image.scale, orientation: image.imageOrientation)
    
    return image
}


func sdpFilterCodec(kind: String, codec: String,sdp: RTCSessionDescription)->String{
    var allowed: [String] = []
    let codecRegex =  try! NSRegularExpression(pattern: "a=rtpmap:(\\d+) \(codec)") // Add 98
    let rtxRegex =  try! NSRegularExpression(pattern: "a=fmtp:(\\d+) apt=(\\d+)$") // Add x apt=98
    var isKind : Bool = false
    let lines = sdp.sdp.split(whereSeparator: \.isNewline)
    lines.forEach{(line) in
        //            print("line \(line)")
        if line.starts(with: "m=\(kind) "){
            isKind = true
        }else if line.starts(with: "m="){
            isKind = false;
        }
        // Only accept m = video
        if (isKind){
            _ = NSRange(location: 0, length: line.utf16.count)
            // codec Regex
            let codecResult = getMatchedByGroup(input: String(line), regex:codecRegex)
            if(codecResult.count > 0){
                //                    print("Added Codec: \(codecResult[0])" )
                allowed.append(codecResult[0])
            }
            
            // rtx Regex
            let rtxResult = getMatchedByGroup(input: String(line), regex:rtxRegex)
            if(rtxResult.count > 0 && rtxResult.count == 2){
                if(allowed.contains(rtxResult[1])){
                    //                        print("Added RTX: \(rtxResult[0])" )
                    allowed.append(rtxResult[0])
                }
            }
            
        }
    }
    
    // Added Allowed.
    let skipRegex =  try! NSRegularExpression(pattern: "a=(fmtp|rtcp-fb|rtpmap):([0-9]+)");
    let videoRegex =  try! NSRegularExpression(pattern: "(m=\(kind) .*?)( ([0-9]+))*\\s*$");
    var sdp: String = ""
    isKind = false
    var allowedStr: String = "";
    for a in allowed{
        allowedStr.append("\(a) ")
    }
    allowedStr = allowedStr.trimmingCharacters(in: .whitespacesAndNewlines)
    
    for line in lines{
        if(line.starts(with: "m=\(kind) ")){
            isKind = true
        }else if (line.starts(with: "m=")){
            isKind = false
        }
        
        if isKind{
            let skipMatch = getMatchedByGroup(input: String(line), regex: skipRegex)
            let videoMatch = getMatchedByGroup(input: String(line), regex: videoRegex)
            //                print("Skip Match: \(skipMatch)")
            if(skipMatch.count > 2 && !allowed.contains(skipMatch[2])){
                //                    print("Ignore \(line)")
                continue // Ignore
            }else if(videoMatch.count > 0 ){
                //                    print("Before Change: \(line)")
                sdp.append("\(line)\n")
                //                    print("After Change: \(allowedStr)")
            }else{
                sdp.append("\(line)\n")
            }
        }else{
            sdp.append("\(line)\n")
        }
    }
    //    print(sdp)
    return sdp
}
