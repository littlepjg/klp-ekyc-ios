//
//  BorderedButton.swift
//  EKycSDK
//
//  Created by Gia Tu Nguyen on 22/09/2021.
//

import Foundation
import UIKit

class BorderedButton: UIButton{
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.borderWidth = 1.0
//        layer.borderColor = tintColor.cgColor
        layer.cornerRadius = 10
        clipsToBounds = true
        contentEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
    }
}

class CircularImageView: UIImageView{
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = self.frame.size.height / 2
        self.clipsToBounds = true
        //        self.layer.borderColor = UIColor.init(red: 2, green: 253, blue: 174).cgColor
        //        self.layer.borderWidth = 1
    }
}

class UnderlineLabel: UILabel{
    override var text: String? {
        didSet {
            guard let text = text else { return }
            let textRange = NSRange(location: 0, length: text.count)
            let attributedText = NSMutableAttributedString(string: text)
            attributedText.addAttribute(.underlineStyle,
                                        value: NSUnderlineStyle.single.rawValue,
                                        range: textRange)
            // Add other attributes if needed
            self.attributedText = attributedText
        }
    }
}


class CircularButton: UIButton{
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = self.frame.size.height / 2
        self.clipsToBounds = true
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.white.cgColor
    }
}

class CircularView: UIView{
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = self.frame.size.height / 2
        self.clipsToBounds = true
    }
}

class ElementView: UIView{
   
    let titleUI: UILabel = {
        let uiLabel = UILabel()
        return uiLabel
    }()
    
    let valueUI: UILabel = {
        let valueLabel = UILabel()
        return valueLabel
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.addSubview(titleUI)
        self.addSubview(valueUI)
        valueUI.numberOfLines = 3
        NSLayoutConstraint.activate([
                                        titleUI.topAnchor.constraint(equalTo: self.topAnchor),
                                        titleUI.bottomAnchor.constraint(equalTo: self.bottomAnchor),
                                        titleUI.leftAnchor.constraint(equalTo: self.leftAnchor),
                                        titleUI.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 180),
                                        valueUI.topAnchor.constraint(equalTo: self.topAnchor),
                                        valueUI.bottomAnchor.constraint(equalTo: self.bottomAnchor),
                                        valueUI.rightAnchor.constraint(equalTo: self.rightAnchor),
                                        valueUI.leftAnchor.constraint(equalTo: self.rightAnchor, constant: 180)])
    }

    
}

class BorderView: UIView{
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius =  self.frame.size.height / 15 < 20 ? 20 :  self.frame.size.height / 15
        self.clipsToBounds = true
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.init(red: 2, green: 253, blue: 174).cgColor
    }
}

class UnderlineBottomView: UIView{
    override func layoutSubviews() {
        super.layoutSubviews()
        addBorders(view: self, edges: .bottom, color: .white, thickness: 1)
    }
}


final class ContentSizedTableView: UITableView {
    override var contentSize:CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }
}

@discardableResult
func addBorders(view: UIView,
                edges: UIRectEdge,
                color: UIColor,
                inset: CGFloat = 0.0,
                thickness: CGFloat = 1.0) -> [UIView] {
    
    var borders = [UIView]()
    
    @discardableResult
    func addBorder(formats: String...) -> UIView {
        let border = UIView(frame: .zero)
        border.backgroundColor = color
        border.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(border)
        view.addConstraints(formats.flatMap {
                                NSLayoutConstraint.constraints(withVisualFormat: $0,
                                                               options: [],
                                                               metrics: ["inset": inset, "thickness": thickness],
                                                               views: ["border": border]) })
        borders.append(border)
        return border
    }
    
    
    if edges.contains(.top) || edges.contains(.all) {
        addBorder(formats: "V:|-0-[border(==thickness)]", "H:|-inset-[border]-inset-|")
    }
    
    if edges.contains(.bottom) || edges.contains(.all) {
        addBorder(formats: "V:[border(==thickness)]-0-|", "H:|-inset-[border]-inset-|")
    }
    
    if edges.contains(.left) || edges.contains(.all) {
        addBorder(formats: "V:|-inset-[border]-inset-|", "H:|-0-[border(==thickness)]")
    }
    
    if edges.contains(.right) || edges.contains(.all) {
        addBorder(formats: "V:|-inset-[border]-inset-|", "H:[border(==thickness)]-0-|")
    }
    
    return borders
}


public extension UIViewController
{
    func startAvoidingKeyboard()
    {
        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(onKeyboardFrameWillChangeNotificationReceived(_:)),
                         name: UIResponder.keyboardWillChangeFrameNotification,
                         object: nil)
    }

    func stopAvoidingKeyboard()
    {
        NotificationCenter.default
            .removeObserver(self,
                            name: UIResponder.keyboardWillChangeFrameNotification,
                            object: nil)
    }

    @objc
    private func onKeyboardFrameWillChangeNotificationReceived(_ notification: Notification)
    {
        guard
            let userInfo = notification.userInfo,
            let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        else {
            return
        }

        let keyboardFrameInView = view.convert(keyboardFrame, from: nil)
        let safeAreaFrame = view.safeAreaLayoutGuide.layoutFrame.insetBy(dx: 0, dy: -additionalSafeAreaInsets.bottom)
        let intersection = safeAreaFrame.intersection(keyboardFrameInView)

        let keyboardAnimationDuration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey]
        let animationDuration: TimeInterval = (keyboardAnimationDuration as? NSNumber)?.doubleValue ?? 0
        let animationCurveRawNSN = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
        let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
        let animationCurve = UIView.AnimationOptions(rawValue: animationCurveRaw)

        UIView.animate(withDuration: animationDuration,
                       delay: 0,
                       options: animationCurve,
                       animations: {
            self.additionalSafeAreaInsets.bottom = intersection.height
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
}
