//
//  CustomizeAnimation.swift
//  EKycSDK
//
//  Created by Gia Tu Nguyen on 26/09/2021.
//

import Foundation
import UIKit

func getRotationAnimation() -> CABasicAnimation{
    let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation")
    rotation.toValue = NSNumber(value: M_PI * 2)
    rotation.duration = 10
    rotation.isCumulative = true
    rotation.repeatCount = FLT_MAX
    return rotation
}
