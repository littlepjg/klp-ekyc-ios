//
//  LivenessViewController.swift
//  EKycSDK
//
//  Created by Gia Tu Nguyen on 23/09/2021.
//

import UIKit
import AVFoundation
import WebRTC

class LivenessViewController: BaseViewController {
    
    private var signalingConnected: Bool = false
    private var hasLocalSdp: Bool = false
    private var localCandidateCount: Int = 0
    private var hasRemoteSdp: Bool = false
    private var remoteCandidateCount: Int = 0
    private var speakerOn: Bool = false
    private var mute: Bool = false
    private var isConnected = false
    
    @IBOutlet weak var ivReview: CircularImageView!
    @IBOutlet weak var localVideoView: CircularView!
    @IBOutlet weak var lblGuide: UILabel!
    // WebRTC Suff
    
    @IBOutlet weak var btnNextOrClose: CircularButton!
    @IBOutlet weak var btnRetry: CircularButton!
    @IBOutlet weak var ivProgressing: UIImageView!
    @IBOutlet weak var ivGuide1: UIImageView!
    @IBOutlet weak var ivGuide2: UIImageView!
    @IBOutlet weak var ivGuide3: UIImageView!
    private var isSucceeded = false
    private static let factory: RTCPeerConnectionFactory = {
        RTCInitializeSSL()
        let videoEncoderFactory = RTCDefaultVideoEncoderFactory()
        let videoDecoderFactory = RTCDefaultVideoDecoderFactory()
        return RTCPeerConnectionFactory(encoderFactory: videoEncoderFactory, decoderFactory: videoDecoderFactory)
    }()
    
    private var peerConnection: RTCPeerConnection?
    private var videoCapturer: RTCCameraVideoCapturer?
    private var localVideoTrack: RTCVideoTrack?
    private var dataChannel: RTCDataChannel?
    private var localStream: RTCMediaStream?
    
    
    @IBAction func onBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("View Will Appear")
        navigationController?.setNavigationBarHidden(true, animated: animated)
        if(app.session == nil || app.session!.isEmpty){
            showAlert(message: "Session của bạn đã hết hạn, vui lòng thử lại...", onOK:{
                self.navigationController?.popToRootViewController(animated: true)
            }, onCancel: nil)
        }
    }
    
    
    override func viewDidLoad() {
        print("View Did Load")
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        start()
    }
    
    // my selector that was defined above
    @objc func willEnterForeground() {
        // do stuff
        print("Enter the fore ground...")
        stop()
        start()
    }
    
    func start(){
        showHideView(true)
        initializePeerConnections() // ice, constraints, ..
        setupSurface() // ==> Call: createVideoTrackFromCameraAndShowIt() // done
        initializeDataChanel() // done
        startStreamingVideo() // done
        negotiate()
        // Do any additional setup after loading the view.
    }
    
    func showHideView(_ isStart: Bool){
        DispatchQueue.main.async {
            self.isConnected = false
            if isStart{
                playVibrate()
                self.lblGuide.textColor = UIColor.white
                //                self.localVideoView.isHidden = false
                self.lblGuide.text = "Hệ thống đang thiết lập, vui lòng đợi giây lát..."
                self.ivReview.isHidden = true
                self.btnRetry.isHidden = true
                self.btnNextOrClose.isHidden = true
                self.ivProgressing.layer.add(getRotationAnimation(), forKey: "ProgressingAnimation")
                self.ivGuide1.layer.opacity = 0.2
                self.ivGuide2.layer.opacity = 0.2
                self.ivGuide3.layer.opacity = 0.2
                
                self.ivGuide1.layer.borderWidth = 0
                self.ivGuide2.layer.borderWidth = 0
                self.ivGuide3.layer.borderWidth = 0
                
            }else{
                //                self.localVideoView.isHidden = true
                self.btnRetry.isHidden = false
                self.ivProgressing.layer.removeAllAnimations()
                if self.isSucceeded{
                    self.lblGuide.textColor = UIColor.green
                    self.btnNextOrClose.isHidden = false
                    self.ivReview.isHidden = false
                    // Auto next
                    self.doNextOrClose()
                }else{
                    self.lblGuide.textColor = UIColor.red
                    self.btnNextOrClose.isHidden = true
                }
                
            }
        }
    }
    
    func stop(){
        self.showHideView(false)
        self.dataChannel?.close()
        self.peerConnection?.close()
    }
    
    func setupSurface(){
        print("setupSurface...")
        #if arch(arm64)
        // Using metal (arm64 only)
        let localRenderer = RTCMTLVideoView(frame: self.localVideoView?.frame ?? CGRect.zero)
        localRenderer.videoContentMode = .scaleAspectFill
        localRenderer.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        #else
        // Using OpenGLES for the rest
        let localRenderer = RTCEAGLVideoView(frame: self.localVideoView?.frame ?? CGRect.zero)
        localRenderer.transform = CGAffineTransformMakeScale(-1.0, 1.0)
        #endif
        
        self.createVideoTrackFromCameraAndShowIt(renderer: localRenderer)
        
        if let localVideoView = self.localVideoView{
            self.embedView(localRenderer, into: localVideoView)
        }
    }
    
    
    func createVideoTrackFromCameraAndShowIt(renderer: RTCVideoRenderer){
        let streamId = "ios-stream"
        let videoSource = LivenessViewController.factory.videoSource()
        videoSource.adaptOutputFormat(toWidth: 720, height: 1080, fps: 15)
        self.localVideoTrack = LivenessViewController.factory.videoTrack(with: videoSource, trackId: "video0")
        self.peerConnection!.add(self.localVideoTrack!,streamIds: [streamId])
        
        self.videoCapturer = RTCCameraVideoCapturer(delegate: videoSource)
        let frontCamera = (RTCCameraVideoCapturer.captureDevices().first { $0.position == .front })
        
        // choose highest res => last. choose lowest res => first.
        let format = (RTCCameraVideoCapturer.supportedFormats(for: frontCamera!).sorted { (f1, f2) -> Bool in
            let width1 = CMVideoFormatDescriptionGetDimensions(f1.formatDescription).width
            let width2 = CMVideoFormatDescriptionGetDimensions(f2.formatDescription).width
            return width1 < width2
        }).last
        
        // choose highest fps
        let fps = (format!.videoSupportedFrameRateRanges.sorted { return $0.maxFrameRate < $1.maxFrameRate }.last)
        
        self.videoCapturer!.startCapture(with: frontCamera!, format: format!, fps:  Int(fps!.maxFrameRate) > 15 ? 15 :  Int(fps!.maxFrameRate), completionHandler:nil)
        self.localVideoTrack?.add(renderer)
    }
    
    func initializePeerConnections(){
        print("initializePeerConnections...")
        let config = RTCConfiguration()
        config.iceServers = [RTCIceServer(urlStrings: Config.default.webRTCIceServers, username: "kalapa", credential: "kalapa123456")]
        config.iceTransportPolicy = .relay
        config.sdpSemantics = .unifiedPlan
        
        
        // Define media constraints. DtlsSrtpKeyAgreement is required to be true to be able to connect with web browsers.
        let constraints = RTCMediaConstraints(mandatoryConstraints: nil,
                                              optionalConstraints: ["DtlsSrtpKeyAgreement":kRTCMediaConstraintsValueTrue])
        let peerConnection = LivenessViewController.factory.peerConnection(with: config, constraints: constraints, delegate: self)
        
        self.peerConnection = peerConnection
    }
    
    func initializeDataChanel(){
        let config = RTCDataChannelConfiguration()
        self.dataChannel = self.peerConnection!.dataChannel(forLabel: "WebRTCData", configuration: config)
        self.dataChannel?.delegate = self
    }
    
    func startStreamingVideo(){
        self.localStream = LivenessViewController.factory.mediaStream(withStreamId: "ios-local-stream")
        self.localStream?.addVideoTrack(self.localVideoTrack!)
    }
    
    func throwExceptionIfWaitedForTooLong(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
            // Do something after 10s
            if(!self.isConnected){
                self.lblGuide.text = "Không thể kết nối do đường truyền không ổn định, vui lòng thử lại..."
                self.onDone()
            }
        }
    }
    
    func negotiate(){
        print("Negotiate...")
        self.throwExceptionIfWaitedForTooLong()
        let mediaConstrains = [kRTCMediaConstraintsOfferToReceiveAudio: kRTCMediaConstraintsValueFalse,
                               kRTCMediaConstraintsOfferToReceiveVideo: kRTCMediaConstraintsValueTrue]
        let constrains = RTCMediaConstraints(mandatoryConstraints: mediaConstrains, optionalConstraints: nil)
        self.peerConnection!.offer(for: constrains, completionHandler: { (sdp, error) in
            guard let sdp = sdp else{
                return
            }
            print("Set Local Description...")
            self.peerConnection?.setLocalDescription(sdp, completionHandler: {(error) in
                if error != nil{
                    print("Something went wrong \(String(describing: error))")
                }
            })
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                do {
                    self.checkIfSessionExpired(self.app.session!)
                    let filteredSdp = try sdpFilterCodec(kind: "video", codec: "VP8/90000", sdp: self.peerConnection!.localDescription!)
                    let type = self.peerConnection!.localDescription!.type.rawValue == 0 ? "offer": "answer"
                    print("Normally send Offer...")
                    
                    self.doOfferWebRtc(sdp: filteredSdp, sdp_type: type, session_id: self.app.session!, onCompletion: { offerResponse in
                        let remoteSDP = RTCSessionDescription(type: RTCSdpType(rawValue: 2)!, sdp: offerResponse!.sdp)
                        self.isConnected = true
                        self.peerConnection?.setRemoteDescription( remoteSDP, completionHandler:{error in
                            if error != nil{
                                print("Something went wrong \(String(describing: error))")
                            }
                        })
                    })
                }catch{
                    print("Errorrrr : \(error.localizedDescription)")
                }
            }
        })
        
    }
    
    private func embedView(_ view: UIView, into containerView: UIView) {
        containerView.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        containerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|",
                                                                    options: [],
                                                                    metrics: nil,
                                                                    views: ["view":view]))
        
        containerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|",
                                                                    options: [],
                                                                    metrics: nil,
                                                                    views: ["view":view]))
        containerView.layoutIfNeeded()
    }
    
    private func onDone(){
        // This called when done.
        self.showHideView(false)
        self.videoCapturer?.stopCapture()
    }
    private func onError(){
        DispatchQueue.main.async {
            self.lblGuide.text = "Đã xảy ra lỗi khi xác thực, vui lòng thực hiện lại :("
            self.onDone()
        }
    }
    
    @IBAction func onNextOrClose(_ sender: Any) {
        self.doNextOrClose()
    }
    
    private func doNextOrClose(){
        // if success => Move. Otherwise close
        print("Should Next...")
        print("Should Close...")
        DispatchQueue.main.async {
            let app = UIApplication.shared.delegate as! AppDelegate
            app.faceImage = self.ivReview.image
            //            let resultViewController = ResultViewController(session: self.session)
            let resulViewControllerv3 = OverviewViewController()
            self.navigationController?.pushViewController(resulViewControllerv3, animated: true)
        }
    }
    
    @IBAction func onRetryPressed(_ sender: Any) {
        stop()
        start()
    }
    
    // MARK: PROCESS LIVENESS
    private func onLivenessProcessing(_ livenessMessage: LivenessMessage){
        if livenessMessage.base64_image != nil{
            // Parse Base 64
            if livenessMessage.status == LIVENESS_EXPIRED{
                
            }else if livenessMessage.status == LIVENESS_VERIFIED || livenessMessage.status == LIVENESS_FAILED{
                // Saved image and show it
                if livenessMessage.base64_image != nil{
                    let dataDecoded: Data = Data(base64Encoded: livenessMessage.base64_image!)!
                    let decodedImage : UIImage = UIImage(data: dataDecoded)!
                    DispatchQueue.main.async {
                        self.ivReview.image = decodedImage
                    }
                    //                    print(livenessMessage.base64_image)
                }
                self.onDone()
            }
        }
        DispatchQueue.main.async {
            if livenessMessage.status == LIVENESS_VERIFIED || livenessMessage.status == LIVENESS_FAILED || livenessMessage.status == LIVENESS_EXPIRED{
                if(livenessMessage.status == LIVENESS_VERIFIED){
                    playVibrate()
                    self.isSucceeded = true
                    self.ivGuide3.layer.borderWidth = 0
                }else{
                    playVibrate()
                    self.isSucceeded = false
                }
                self.onDone()
            }
            self.lblGuide.text = getLivenessMessage(status: livenessMessage.status, input: livenessMessage.message)
            if (livenessMessage.message == "HoldSteady2Second" || livenessMessage.message == "HoldSteady3Second"){
                playVibrate()
                self.ivGuide1.layer.opacity = 1.0
                self.ivGuide1.layer.borderWidth = 1
                self.ivGuide1.layer.borderColor = UIColor.init(red: 2, green: 253, blue: 174).cgColor
            }else if (livenessMessage.message == "NodeHead"){
                playVibrate()
                self.ivGuide2.layer.opacity = 1.0
                self.ivGuide2.layer.borderWidth = 1
                self.ivGuide2.layer.borderColor = UIColor.init(red: 2, green: 253, blue: 174).cgColor
                self.ivGuide1.layer.borderWidth = 0
            }else if (livenessMessage.message == "ShakeHead"){
                playVibrate()
                self.ivGuide3.layer.opacity = 1.0
                self.ivGuide3.layer.borderWidth = 1
                self.ivGuide3.layer.borderColor = UIColor.init(red: 2, green: 253, blue: 174).cgColor
                self.ivGuide2.layer.borderWidth = 0
            }
            //            self.imgGuide.image = UIImage(named: getIconGuider(input: livenessMessage.message))
        }
    }
    
    
}
extension LivenessViewController: RTCPeerConnectionDelegate{
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange stateChanged: RTCSignalingState) {
        print("PeerConnection State Did Change \(stateChanged)")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didAdd stream: RTCMediaStream) {
        print("PeerConnection Did Add Stream \(stream)")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove stream: RTCMediaStream) {
        print("PeerConnection Did Remove Stream \(stream)")
    }
    
    func peerConnectionShouldNegotiate(_ peerConnection: RTCPeerConnection) {
        print("PeerConnection Should Negotiate")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceConnectionState) {
        print("PeerConnection Did Change Connection State: \(newState)")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceGatheringState) {
        print("PeerConnection Did Change Gathering state: \(newState)")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didGenerate candidate: RTCIceCandidate) {
        self.peerConnection!.add(candidate)
        //        print("PeerConnection Did Generate Candidate: \(candidate)")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove candidates: [RTCIceCandidate]) {
        //        print("PeerConnection Did Remove Candidate: \(candidates)")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didOpen dataChannel: RTCDataChannel) {
        print("PeerConnection Did Open DataChannel: \(dataChannel)")
    }
    
}

extension LivenessViewController: RTCDataChannelDelegate{
    func dataChannelDidChangeState(_ dataChannel: RTCDataChannel) {
        print("On DataChange State: \(dataChannel)")
    }
    
    func dataChannel(_ dataChannel: RTCDataChannel, didReceiveMessageWith buffer: RTCDataBuffer) {
        guard let incomingMessage = buffer.data.base64EncodedString().fromBase64() else{
            return
        }
        //        print("On Receive Message : \(incomingMessage)")
        // Message Processes
        if incomingMessage.contains("&&"){
            let messages = incomingMessage.components(separatedBy: "&&")
            if(messages.count > 1){
                let jsonData = messages[1].replacingOccurrences(of: "'", with: "\"").data(using: String.Encoding.utf8)
                do{
                    let livenessMessage = try JSONDecoder().decode(LivenessMessage.self, from: jsonData!)
                    self.onLivenessProcessing(livenessMessage)
                }catch{
                    print(error.localizedDescription)
                    self.onError()
                    
                }
            }
        }
        
    }
    
}
