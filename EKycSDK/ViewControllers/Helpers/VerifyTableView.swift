//
//  VerifyTableView.swift
//  EKycSDK
//
//  Created by Gia Tu Nguyen on 28/09/2021.
//

import Foundation
import UIKit
class VerifyTableView: UIViewController{
    let verifyObj: VerifyObj
    
    
    
    init(verifyObj: VerifyObj) {
        self.verifyObj = verifyObj
        super.init(nibName: String(describing: ResultViewController.self), bundle: Bundle.main)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
