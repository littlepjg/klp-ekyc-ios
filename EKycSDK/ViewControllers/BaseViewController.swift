//
//  BaseViewController.swift
//  EKycSDK
//
//  Created by Gia Tu Nguyen on 22/09/2021.
//

import UIKit
import Alamofire
import AVFoundation
import Reachability

class BaseViewController: UIViewController {
    let apiQueue = DispatchQueue(label: "API",qos:.background)
    
    let app = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func showAlert(message:String, onOK: (()->Void)? = nil,onCancel: (()->Void)? = nil){
        let alert = UIAlertController.init(title: "Xảy ra lỗi", message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Đồng ý", style: .default) { (alert) in
            //            self.navigationController?.popToRootViewController(animated: true)
            onOK?()
            SKActivityIndicator.dismiss()
        }
        if onCancel != nil{
            let cancel = UIAlertAction(title: "Hủy bỏ", style: .cancel) { (alert) in
                //            self.navigationController?.popToRootViewController(animated: true)
                onCancel?()
                SKActivityIndicator.dismiss()
            }
        }
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertInfo(message:String){
        let alert = UIAlertController.init(title: "Thông báo", message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "OK", style: .default) { (alert) in
        }
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    func checkIfSessionExpired(_ session: String) -> Void{
        if(Reachability.isConnectedToNetwork()){
            let url = KLP_SERVICE_SERVER + KLP_SERVICE_PING
            AF.request(url,method: .get,parameters: nil,encoding: JSONEncoding.default, headers: ["Authorization":session])
                .response{ [self] (result) in
                    if let httpStatus = result.response{
                        if  httpStatus.statusCode == 200{
                            print("SESSION IS OK!")
                            return
                        }else{
                            print("SESSION IS EXPIRED! \(httpStatus.statusCode)")
                            showAlert(message: "Phiên làm việc của bạn đã hết hạn, vui lòng thực hiện lại từ đầu...",onOK: {
                                self.navigationController?.popToRootViewController(animated: true)
                            })
                        }
                    }else{
                        showAlert(message: "Phiên làm việc của bạn đã hết hạn, vui lòng thực hiện lại từ đầu...",onOK: {
                            self.navigationController?.popToRootViewController(animated: true)
                        })
                    }
                }
        }else{
            showAlert(message: "Không có kết nối Internet hoặc đường truyền không ổn định. Vui lòng thử lại sau")
        }
        
    }
    
    func imageFromSampleBuffer(sampleBuffer:CMSampleBuffer) -> UIImage{
        let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)
        CVPixelBufferLockBaseAddress(imageBuffer!, [])
        let baseAddress = CVPixelBufferGetBaseAddress(imageBuffer!)
        let bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer!)
        let width = CVPixelBufferGetWidth(imageBuffer!)
        let height = CVPixelBufferGetHeight(imageBuffer!)
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageByteOrderInfo.order32Little.rawValue | CGImageAlphaInfo.premultipliedFirst.rawValue)
        let context = CGContext(data: baseAddress, width: width, height: height, bitsPerComponent: 8, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)
        let quartzImage = context!.makeImage();
        CVPixelBufferUnlockBaseAddress(imageBuffer!,[]);
        // Create an image object from the Quartz image
        let image = UIImage.init(cgImage: quartzImage!, scale: 1.0, orientation: .right)
        return image;
    }
    
    func callGetSession(token: String,
                        onCompletion: ((SessionModel?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil, onExpired: ((Any?) -> Void)? = nil){
        if Reachability.isConnectedToNetwork() {
            let url = KLP_SERVICE_SERVER + KLP_SERVICE_GET_SESSION
            AF.request(url,method: .post,parameters: ["app_token":token,"allow_sdk_full_results":true,"session_plan":"PLAN_FULL"],encoding: JSONEncoding.default, headers: nil)
                .response{ (result) in
                    if let httpStatus = result.response{
                        if  httpStatus.statusCode == 200{
                            let decoder = JSONDecoder()
                            do{
                                let obj = try decoder.decode(SessionModel.self, from: result.data!)
                                //                            print("On Completion")
                                //                            print(obj)
                                onCompletion?(obj)
                            }catch{
                                print("Error: \(error)")
                            }
                            
                        }else{
                            onExpired?(result.data!)
                        }
                    }
                }
        }else{
            showAlert(message: "Không có kết nối Internet hoặc đường truyền không ổn định. Vui lòng thử lại sau")
            onExpired?("No Internet Connection")
        }
        
    }
    
    func backToRoot(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func requestCheckingFrontCard(session: String, frontCard: UIImage, onCompletion: ((IDCard) -> Void)? = nil,
                                  onNG: ((IDCard) -> Void)? = nil,
                                  onError: ((Any?) -> Void)? = nil){
        requestCheckCard(session: session, isFront: true, cardImage: frontCard, onCompletion: onCompletion,onNG: onNG,onError: onError)
    }
    
    func requestCheckingBackCard(session: String, backCard: UIImage, onCompletion: ((IDCard) -> Void)? = nil,
                                 onNG: ((IDCard) -> Void)? = nil,
                                 onError: ((Any?) -> Void)? = nil){
        requestCheckCard(session: session, isFront: false, cardImage: backCard, onCompletion: onCompletion,onNG: onNG,onError: onError)
    }
    
    func requestCheckCard(session: String,isFront: Bool, cardImage: UIImage, onCompletion: ((IDCard) -> Void)? = nil,
                          onNG: ((IDCard) -> Void)? = nil,
                          onError: ((Any?) -> Void)? = nil){
        checkIfSessionExpired(session)
        let url = KLP_SERVICE_SERVER + ( isFront ? KLP_SERVICE_CHECK_FRONT : KLP_SERVICE_CHECK_BACK )
        let imageData = cardImage.jpegData(compressionQuality: 1.0)
        let headers: HTTPHeaders = ["Authorization":session ]
        AF.upload(multipartFormData: { multiPart in
            if let data = imageData{
                multiPart.append(data, withName: "image", fileName: "\(isFront ? "front_card_" : "back_card_")\(getCurrentTimeMillis()).jpeg", mimeType: "image/jpg")
            }
        }, to: url,headers: headers)
        .uploadProgress(queue: .main, closure: { progress in
            //Current upload progress of file
            print("Upload Progress \(isFront ? "Front Card" : "Back Card" ): \(progress.fractionCompleted)")
        })
        .responseJSON(completionHandler: { data in
            //Do what ever you want to do with response
            let decoder = JSONDecoder()
            
            do{
                let obj = try decoder.decode(IDCard.self, from: data.data!)
                dump(obj)
                if(obj.error.code == 0){
                    onCompletion?(obj)
                }else{
                    onNG?(obj)
                }
                
            }catch{
                print("Error: \(error)")
                onError?(error)
            }
        })
    }
    
    
    func doOfferWebRtc(sdp: Any,sdp_type: Any, session_id: String, onCompletion: ((OfferResponse?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        let url = KLP_SERVICE_LIVENESS_OFFER
        //        print("On Request: Session: \(session_id)")
        AF.request(url, method: .post, parameters: [
            "sdp": sdp,
            "type": sdp_type,
            "session_id": session_id,
            "video_transform":"none",
            "is_save_result":true
        ],encoding: JSONEncoding.default,headers: nil)
        .response{ (result) in
            if let httpStatus = result.response{
                if  httpStatus.statusCode == 200{
                    let decoder = JSONDecoder()
                    do{
                        let obj = try decoder.decode(OfferResponse.self, from: result.data!)
                        //                        print("On Completion \(obj.sdp)")
                        //                        print("On Completion \(obj.type)")
                        onCompletion?(obj)
                    }catch{
                        print("Error: \(error)")
                        onError?(error)
                    }
                }else{
                    print("On Error:  \(httpStatus.statusCode)")
                }
            }
        }
    }
    
    
    //    // MARK: init Signaling Client from anywhere
    //    func buildSignalingClient() -> SignalingClient {
    //
    //        // iOS 13 has native websocket support. For iOS 12 or lower we will use 3rd party library.
    //        let webSocketProvider: WebSocketProvider
    //
    //        if #available(iOS 13.0, *) {
    //            webSocketProvider = NativeWebSocket(url: Config.default.signalingServerUrl)
    //        } else {
    //            webSocketProvider = StarscreamWebSocket(url: Config.default.signalingServerUrl)
    //        }
    //
    //        return SignalingClient(webSocket: webSocketProvider)
    //    }
    
    func getData(session: String, type: String, onCompletion: ((Data?)->Void)? = nil, onNoData: (()->Void)? = nil, onError: ((Any?)->Void)? = nil ){
        let url = KLP_SERVICE_SERVER + KLP_SERVICE_GET_DATA
            AF.request(url,method: .get,parameters: ["type":type],headers: ["Authorization": session]).response{ (result) in
                 if let httpResponse = result.response{
                    if httpResponse.statusCode == 200 {
                        // on Completion
//                        print("\(type) data: \(result.data)" )
//                        print("\(type) data: \(result.data?.base64EncodedString())" )
                        onCompletion?(result.data)
                    }else if httpResponse.statusCode == 204 {
                        // No Data.
                        onNoData?()
                    }else{
                        // on Error
                        onError?(httpResponse)
                    }
                }
            }
    }
    
    func handleError(error: Any?){
        showAlert(message: "Hệ thống đã xảy ra lỗi bất thường, vui lòng thử lại sau", onOK: {
            self.navigationController?.popToRootViewController(animated: true)
        }, onCancel: nil)
    }
}
