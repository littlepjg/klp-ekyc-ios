//
//  ResultScreenViewController.swift
//  EKycSDK
//
//  Created by Gia Tu Nguyen on 30/09/2021.
//

import UIKit

class ResultScreenViewController: BaseViewController {
    let session: String
    
    @IBOutlet weak var ivFace: CircularImageView!
    
    init(session: String) {
        self.session = session
        super.init(nibName: String(describing: ResultScreenViewController.self), bundle: Bundle.main)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
