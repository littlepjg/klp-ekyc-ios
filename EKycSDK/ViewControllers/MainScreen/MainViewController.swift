//
//  MainViewController.swift
//  EKycSDK
//
//  Created by Gia Tu Nguyen on 22/09/2021.
//

import UIKit
class MainViewController: BaseViewController,UITextFieldDelegate {
    var token = ""
    var isTokenGettinng: Bool = false
    var isFirstDone: Bool = false
    
    @IBOutlet weak var edtToken: UITextField!
//    private lazy var ocrScreenViewController = OcrScreenViewController(session: self.session)
//    private lazy var resultViewController = ResultViewController(session: "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxIiwiaWF0IjoxNjMyNzk0NDc3LCJleHAiOjE2MzI3OTUwNzd9.iSzfoaBH3ptC8OpB97Hbu1twnFAdPo62Wz9eoN1IedMNxcjfoeN8Z8LlpZpvgacrjmCn54tHlEuTLaaO8IInZw")
//    private lazy var resultViewController2 = ResultScreenViewController(session: "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxIiwiaWF0IjoxNjMyNzk0NDc3LCJleHAiOjE2MzI3OTUwNzd9.iSzfoaBH3ptC8OpB97Hbu1twnFAdPo62Wz9eoN1IedMNxcjfoeN8Z8LlpZpvgacrjmCn54tHlEuTLaaO8IInZw")
//        private lazy var resultViewController3 = OverviewViewController(session: "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxIiwiaWF0IjoxNjMyNzk0NDc3LCJleHAiOjE2MzI3OTUwNzd9.iSzfoaBH3ptC8OpB97Hbu1twnFAdPo62Wz9eoN1IedMNxcjfoeN8Z8LlpZpvgacrjmCn54tHlEuTLaaO8IInZw")
//    private lazy var livenessViewController = LivenessViewController(session: self.session)
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        print("On View Will APPEAR")
//        self.removeAllNavigation()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.edtToken.delegate = self
        self.edtToken.layer.cornerRadius = 10
        self.edtToken.layer.borderWidth = 1
        self.edtToken.layer.borderColor = UIColor(red: 2, green: 253, blue: 174, alpha: 1).cgColor
        
        self.edtToken.attributedPlaceholder =
            NSAttributedString(string: "Enter your provided Token here", attributes: [NSAttributedString.Key.foregroundColor : UIColor.gray])
        
    }
    
    private func removeAllNavigation(){
        guard let navigationController = self.navigationController else { return }
        var navigationArray = navigationController.viewControllers // To get all UIViewController stack as Array
        let temp = navigationArray.last
        navigationArray.removeAll()
        navigationArray.append(temp!) //To remove all previous UIViewController except the last one
        self.navigationController?.viewControllers = navigationArray
    }
    
    @IBAction func onTestTouch(_ sender: Any) {
        self.isTokenGettinng = true
        SKActivityIndicator.spinnerColor(UIColor.white)
        SKActivityIndicator.spinnerStyle(.spinningCircle)
        SKActivityIndicator.statusTextColor(UIColor.white)
        SKActivityIndicator.show("Vui lòng đợi giây lát,\n Hệ thống đang kết nối...")
        callGetSession(token: "5bb42ea331ee010001a0b7d71dba5816861b4fa7ad5aadc567fbc5e9", onCompletion:{result in
            if(self.isTokenGettinng == true){
                SKActivityIndicator.dismiss()
//                    print(result!)
//                    print(result?.token as Any)
//                    self.session = result!.token
                self.app.session = result!.token
                
                self.navigationController?.pushViewController(LivenessViewController(), animated: true)
//
                self.isTokenGettinng = false
            }
                
            // 5bb42ea331ee010001a0b7d7707457da34b2407b89215fbd375f209d
            
        }, onError: { exception in
            self.isTokenGettinng = false
            print(exception as Any)
            SKActivityIndicator.dismiss()
        },onExpired: {error in
            self.isTokenGettinng = false
            print("Expired or unAuthorizationed \(String(describing: error))")
            self.showAlertInfo(message: "Token không đúng hoặc hết hạn,\n vui lòng liên hệ quản trị viên để được hỗ trợ")
            SKActivityIndicator.dismiss()
        }
        )
       
    }
    private func onShouldNext(){
        if(edtToken.hasText){
            self.token = edtToken.text!
            if(self.token) == "kalapa@2021" {
                self.token = "5bb42ea331ee010001a0b7d71dba5816861b4fa7ad5aadc567fbc5e9"
            }
            self.isTokenGettinng = true
            SKActivityIndicator.spinnerColor(UIColor.white)
            SKActivityIndicator.spinnerStyle(.spinningCircle)
            SKActivityIndicator.statusTextColor(UIColor.white)
            SKActivityIndicator.show("Vui lòng đợi giây lát,\n Hệ thống đang kết nối...")
            callGetSession(token: self.token, onCompletion:{result in
                if(self.isTokenGettinng == true){
                    SKActivityIndicator.dismiss()
//                    print(result!)
//                    print(result?.token as Any)
//                    self.session = result!.token
                    self.app.session = result!.token
                    
                    self.navigationController?.pushViewController(OcrScreenViewController(), animated: true)
//
                    self.isTokenGettinng = false
                }
                    
                // 5bb42ea331ee010001a0b7d7707457da34b2407b89215fbd375f209d
                
            }, onError: { exception in
                self.isTokenGettinng = false
                print(exception as Any)
                SKActivityIndicator.dismiss()
            },onExpired: {error in
                self.isTokenGettinng = false
                print("Expired or unAuthorizationed \(String(describing: error))")
                self.showAlertInfo(message: "Token không đúng hoặc hết hạn,\n vui lòng liên hệ quản trị viên để được hỗ trợ")
                SKActivityIndicator.dismiss()
            }
            )
            
        }else{
            let alert = UIAlertController(title: "Thông báo", message: "Vui lòng nhập vào Token được\ncung cấp bởi Kalapa để tiếp tục", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Xác nhận", style:.default, handler: nil))
            self.present(alert,animated: true,completion: nil)
        }
    }
    
    @IBAction func onStartEKyc(_ sender: Any) {
        self.onShouldNext()
    }
    
    private func setupView(){
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(!self.isFirstDone){
            self.isFirstDone = true
            self.onShouldNext()
        }
        view.endEditing(true)
        return false
    }
 
}
