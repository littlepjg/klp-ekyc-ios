//
//  OcrScreenViewController.swift
//  EKycSDK
//
//  Created by Gia Tu Nguyen on 27/09/2021.
//

import UIKit
import AVFoundation

class OcrScreenViewController: BaseViewController,AVCapturePhotoCaptureDelegate {
    var frontImage: UIImage?
    
    var backImage: UIImage?
    
    var capturedImage: UIImage?
    
    var isPreviewing: Bool = false
    
    var isCapturingFront: Bool = true
    
    var isRetry: Bool = false
    
    let dispatchGroup = DispatchGroup()
    
    var timeTakenPhoto: Double?
    var stillImageOutput: AVCapturePhotoOutput!
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    var captureSession: AVCaptureSession?
    
    
    @IBOutlet weak var lblHeader: UILabel!
    
    @IBOutlet weak var ivFrameHolder: UIImageView!
    
    @IBOutlet weak var previewView: BorderView!
    
    @IBOutlet weak var lblGuide: UILabel!
    
    @IBOutlet weak var ivGuide: UIImageView!
    
    @IBOutlet weak var btnCapture: CircularButton!
    
    @IBOutlet weak var btnRetake: UIButton!
    
    @IBOutlet weak var viewGuideHolder: BorderView!
    
    @IBOutlet weak var btnConfirm: CircularButton!
    
    
    
    @IBAction func onBackPressed(_ sender: Any) {
        if(isPreviewing) {
            isPreviewing = false
        }else{
            // Back to Capture Front Image, Or back to Previous Step
            if(self.isCapturingFront){
                self.releaseResource()
                self.navigationController?.popViewController(animated: true)
            }else{
                self.releaseResource()
                self.isCapturingFront = true
            }
        }
        self.onUpdateUI()
    }
    
    @IBAction func onCapturePressed(_ sender: Any) {
        self.isRetry = false
        if #available(iOS 11.0, *){
            let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg]) // Avoid HEIC.
            stillImageOutput.capturePhoto(with: settings, delegate: self)
        }else{
            let settings = AVCapturePhotoSettings.init()
            stillImageOutput.capturePhoto(with: settings, delegate: self)
        }
    }
    
    @available(iOS 11.0, *)
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        guard let imageData = photo.fileDataRepresentation() else {
            print("Error while capturing photo: \(String(describing: error?.localizedDescription))")
            return}
        let image = UIImage(data: imageData)
        self.capturedImage = image
        print("Image Output v11: \(imageData.base64EncodedData())")
        self.onPreview(image!)
    }
    
    
    // Folder 10 => 11
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        if(photoSampleBuffer) != nil{
            let data = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: photoSampleBuffer!, previewPhotoSampleBuffer: previewPhotoSampleBuffer)
            print("Image Output v10: \(data!.base64EncodedData())")
            let image = UIImage.init(data: data!)
            self.onPreview(image!)
        }
    }
    
    private func onBegin(){
        self.releaseResource()
        self.onUpdateUI()
    }
    
    
    private func onUpdateUI(){
        if(!self.isPreviewing){
            if(!self.captureSession!.isRunning){
                self.captureSession!.startRunning()
            }
        }
        DispatchQueue.main.async {
            if(self.isPreviewing){
                self.lblGuide.text = "Đang kiểm tra tính hợp lệ của ảnh... \n Bạn có thể chụp lại hình bất cứ lúc nào."
            }
            self.btnRetake.isHidden = !self.isPreviewing
            self.ivGuide.isHidden = self.isPreviewing
            self.viewGuideHolder.isHidden = self.isPreviewing
            self.btnCapture.isHidden = self.isPreviewing
            if(self.isCapturingFront){
                self.lblHeader.text = "Chụp ảnh CMND/CCCD mặt trước"
                self.ivGuide.image = UIImage(named: "guide_front")
            }else{
                self.lblHeader.text = "Chụp ảnh CMND/CCCD mặt sau"
                self.ivGuide.image = UIImage(named: "guide_back")
            }
        }
    }
    
    private func releaseResource(){
        self.frontImage = nil
        self.backImage = nil
        self.capturedImage = nil
        self.isPreviewing = false
        self.isRetry = false
//        self.btnRetake.isHidden = true
        self.btnConfirm.isHidden = true
        self.btnCapture.isHidden = false
    }
    
    
    private func onRestarting(){
        self.isPreviewing = false
        if(!self.captureSession!.isRunning){
            self.captureSession?.startRunning()
        }
    }
    
    private func onPreview(_ stillImage: UIImage){
        self.isPreviewing = true
        self.timeTakenPhoto = getCurrentTimeMillis()
        onUpdateUI()
        self.captureSession?.stopRunning()
        DispatchQueue.main.async { [self] in
            let timeCheck = self.timeTakenPhoto
            print("Time Check: \(timeCheck) + Time Taken: \(self.timeTakenPhoto)")
            if(self.isCapturingFront){
                self.frontImage = cropToBounds(image: self.capturedImage!, width: 840, height: 640)
              
                self.requestCheckingFrontCard(session: self.app.session!, frontCard: self.frontImage!, onCompletion:  { idCard in
                    // Succeed
                    print("Time Check: \(timeCheck) + Time Taken: \(self.timeTakenPhoto)")
                    if(!self.isRetry && timeCheck == self.timeTakenPhoto){
                        playVibrate()
                        DispatchQueue.main.async {
                            self.lblGuide.text = "Ảnh hợp lệ"
                            self.lblGuide.textColor = UIColor.green
                        }
                        let app = UIApplication.shared.delegate as! AppDelegate
                        app.frontImage = self.frontImage
                        app.frontCard = idCard
                        self.btnConfirm.isHidden = false
                        self.onConfirm()
                    }else{
                        print("Ignore ignored photo")
                    }
                },onNG: { idCard in
                    // NG
                    if(!self.isRetry && timeCheck == self.timeTakenPhoto){
                        playVibrate()
                        DispatchQueue.main.async {
                            self.lblGuide.text = "Ảnh \(self.isCapturingFront ? "mặt trước" : "mặt sau") không hợp lệ:\n\(idCard.error.message)"
                            self.lblGuide.textColor = UIColor.red
                        }
                    }
                    
                },onError: { error in
                    // Failed
                    if(!self.isRetry && timeCheck == self.timeTakenPhoto){
                        DispatchQueue.main.async {
                            self.lblGuide.text = "Xảy ra lỗi, vui lòng thử lại sau"
                            self.btnRetake.isHidden = false
                            self.frontImage = nil
                            self.capturedImage = nil
                            self.onUpdateUI()
                        }
                    }
                })
            }else{
                self.backImage = cropToBounds(image: self.capturedImage!, width: 840, height: 640)
                // Check Back
                self.requestCheckingBackCard(session: self.app.session!, backCard: self.backImage!, onCompletion:  { idCard in
                    // Succeed
                    print("Time Check: \(timeCheck) + Time Taken: \(self.timeTakenPhoto)")
                    if(!self.isRetry && timeCheck == self.timeTakenPhoto){
                        playVibrate()
                        DispatchQueue.main.async {
                            self.lblGuide.text = "Ảnh hợp lệ"
                            self.lblGuide.textColor = UIColor.green
                        }
                        let app = UIApplication.shared.delegate as! AppDelegate
                        app.backImage = self.backImage
                        app.backCard = idCard
                        self.btnConfirm.isHidden = false
                        self.onConfirm()
                    }else{
                         print("Ignore ignored photo")
                    }
                },onNG: { idCard in
                    // NG
                    if(!self.isRetry && timeCheck == self.timeTakenPhoto){
                        playVibrate()
                        DispatchQueue.main.async {
                            self.lblGuide.text = "Ảnh \(self.isCapturingFront ? "mặt trước" : "mặt sau") không hợp lệ:\n\(idCard.error.message)"
                            self.lblGuide.textColor = UIColor.red
                        }
                    }
                },onError: { error in
                    // Failed
                    if(!self.isRetry && timeCheck == self.timeTakenPhoto){
                    DispatchQueue.main.async {
                        self.lblGuide.text = "Xảy ra lỗi, vui lòng thử lại sau"
                        self.btnRetake.isHidden = false
                        self.backImage = nil
                        self.capturedImage = nil
                        self.onUpdateUI()
                        }
                    }
                })
            }
        }
        
    }
    @IBAction func onRetryPressed(_ sender: Any) {
        self.onRetry()
    }
    
    private func onRetry(){
        self.isRetry = true
        self.isPreviewing = false
        self.btnConfirm.isHidden = true
        DispatchQueue.main.async {
            self.lblGuide.text = "Chú ý: Tránh chụp ảnh mờ, thiếu góc, chói sáng"
            self.lblGuide.textColor = UIColor.white
        }
        if(isCapturingFront){
            self.frontImage = nil
            self.capturedImage = nil
        }else{
            self.backImage = nil
            self.capturedImage = nil
        }
        onUpdateUI()
    }
    
    @IBAction func onConfirmPressed(_ sender: Any) {
        self.onConfirm()
    }
    private func onConfirm(){
        self.isRetry = false
        self.btnConfirm.isHidden = true
        self.btnRetake.isHidden = true
        self.btnCapture.isHidden = false
        self.isPreviewing = false
        DispatchQueue.main.async {
            self.lblGuide.text = "Chú ý: Tránh chụp ảnh mờ, thiếu góc, chói sáng"
            self.lblGuide.textColor = UIColor.white
        }
        // Confirm to take back photo or move to next
        if(!isCapturingFront){
            if(self.frontImage != nil && self.backImage != nil){
                onDestroy()
                self.navigationController?.pushViewController(LivenessViewController(), animated: true)
            }else{
                self.onRetry()
            }
        }else{
            self.isCapturingFront = false
            self.backImage = nil
            self.capturedImage = nil
            onUpdateUI()
            // Taking back
        }
    }
    
    private func onDestroy(){
        self.isCapturingFront = true
        self.isPreviewing = false
        self.btnRetake.isHidden = true
        self.btnConfirm.isHidden = true
        self.btnCapture.isHidden = false
        self.ivGuide.isHidden = false
        self.viewGuideHolder.isHidden = false
        self.frontImage = nil
        self.backImage = nil
        self.releaseResource()
        self.onUpdateUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if(app.session == nil || app.session!.isEmpty){
            showAlert(message: "Session của bạn đã hết hạn, vui lòng thử lại...", onOK:{
                self.navigationController?.popToRootViewController(animated: true)
            }, onCancel: nil)
        }
        
        if !self.captureSession!.isRunning {
            captureSession = AVCaptureSession()
            
            captureSession!.sessionPreset = .hd1280x720
            guard let backCamera = AVCaptureDevice.default(for: AVMediaType.video)
            else {
                print("Unable to access back camera!")
                return
            }
            do {
                let input = try AVCaptureDeviceInput(device: backCamera)
                stillImageOutput = AVCapturePhotoOutput()
                if captureSession!.canAddInput(input) && captureSession!.canAddOutput(stillImageOutput) {
                    captureSession!.addInput(input)
                    captureSession!.addOutput(stillImageOutput)
                    setupLivePreview()
                }
            }
            catch let error  {
                print("Error Unable to initialize back camera:  \(error.localizedDescription)")
            }
        }
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    
    // MARK: Initial Camera Session
    func initializeCameraSession(){
        if self.captureSession == nil{
            self.captureSession = AVCaptureSession()
        }
        self.captureSession!.sessionPreset = .hd1280x720
        guard let backCamera = AVCaptureDevice.default(for: AVMediaType.video)else{
            print("Unable to access back camera!")
            return
        }
        do{
            let input = try AVCaptureDeviceInput(device: backCamera)
            self.stillImageOutput = AVCapturePhotoOutput()
            if self.captureSession!.canAddInput(input) && captureSession!.canAddOutput(stillImageOutput){
                captureSession!.addInput(input)
                captureSession!.addOutput(stillImageOutput)
                self.setupLivePreview()
            }
        }catch let error{
            print("Error while intial back camera: \(error.localizedDescription)")
        }
    }
    
    // MARK: Setup Preview Layer
    func setupLivePreview(){
        print("Setup Live Preview....")
        self.videoPreviewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession!)
        self.videoPreviewLayer.videoGravity = .resizeAspectFill
        self.videoPreviewLayer.connection?.videoOrientation = .portrait
        self.previewView.layer.addSublayer(self.videoPreviewLayer)
        
        DispatchQueue.global(qos: .userInitiated).async {
            self.captureSession!.startRunning()
            DispatchQueue.main.async {
                self.videoPreviewLayer.frame = self.previewView.bounds
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if captureSession == nil {
            captureSession = AVCaptureSession()
        }
        captureSession!.sessionPreset = .hd1280x720
        guard let backCamera = AVCaptureDevice.default(for: AVMediaType.video)
        else {
            print("Unable to access back camera!")
            return
        }
        do {
            let input = try AVCaptureDeviceInput(device: backCamera)
            stillImageOutput = AVCapturePhotoOutput()
            if captureSession!.canAddInput(input) && captureSession!.canAddOutput(stillImageOutput) {
                captureSession!.addInput(input)
                captureSession!.addOutput(stillImageOutput)
                setupLivePreview()
                SKActivityIndicator.dismiss()
            }
        }
        catch let error  {
            print("Error Unable to initialize back camera:  \(error.localizedDescription)")
        }
        self.btnRetake.isHidden = true
        self.btnConfirm.isHidden = true
        self.lblGuide.text = ""
        self.lblGuide.text = "Chú ý: Tránh chụp ảnh mờ, thiếu góc, chói sáng"
        onBegin()
        onUpdateUI()
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
