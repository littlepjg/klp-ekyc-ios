//
//  ResultViewController.swift
//  EKycSDK
//
//  Created by Gia Tu Nguyen on 27/09/2021.
//

import UIKit

class ResultViewController: BaseViewController{
    var isConnected = false
    
    var ocrInfo:[Any] = [Any]()
    var verifyInfo:[Any] = [Any]()
    var antifraudInfo:[Any] = [Any]()
    
    var session: String
    private var isVerifyDataFailed: Bool = false
    private var isLogicDataFailed: Bool = false
    private var isSelfieDataFailed: Bool = false
    private var isAntifraudDataFailed: Bool = false
    
    @IBOutlet weak var ivFace: CircularImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Load saved Data: Front / Back / Selfie
//        self.loadLocalData()
        // Get enrich Data: Antifraud, Verify, Logic, DECISION
        self.getEnrichData()
    }
    
    @IBAction func onBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    private func getSelfie(triedAttempt: Int){
        // Get SELFIE
        self.getData(session: self.session, type: "SELFIE",onCompletion: {result in
            let selfieobj = try? JSONDecoder().decode(SelfieObj.self, from: result!)
            // SELFIE UI
            print("Selfie Data: \(selfieobj?.data.isMatched) \(selfieobj?.data.matchingScore) \(selfieobj?.error.message)")
        },onNoData: {
            if(triedAttempt > 3){
                self.isSelfieDataFailed = true
                print("Tried 3 times still no data, give up...")
                return
            }else{
                print("No SELFIE Data, retry in 3 seconds, \(triedAttempt + 1) times")
                DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 3){
                    self.getSelfie(triedAttempt: triedAttempt + 1)
                }
            }
        },onError: { error in
            self.isSelfieDataFailed = true
        })
    }
    
    private func getLogic(triedAttempt: Int){
        // Get LOGIC
        self.getData(session: self.session, type: "LOGIC",onCompletion: {result in
            let logicObj = try? JSONDecoder().decode(LogicObj.self, from: result!)
            // LOGIC UI
            print("Logic Data: \(logicObj?.data.validateProvince.message.vi) \(logicObj?.data.validateProvince.value)")
            print("Logic Data: \(logicObj?.data.validateGender.message.vi) \(logicObj?.data.validateGender.value)")
            print("Logic Data: \(logicObj?.data.validateYob.message.vi) \(logicObj?.data.validateYob.value)")
            print("Logic Data: \(logicObj?.data.validateAddress.message.vi) \(logicObj?.data.validateAddress.value)")
            print("Logic Data: \(logicObj?.data.validateRegisterAge.message.vi) \(logicObj?.data.validateRegisterAge.value)")
            print("Logic Data: \(logicObj?.data.validateExpiry.message.vi) \(logicObj?.data.validateExpiry.value)")
            print("Logic Data: \(logicObj?.data.validateDob.message.vi) \(logicObj?.data.validateDob.value)")
            print("Logic Data: \(logicObj?.data.validateDoi.message.vi) \(logicObj?.data.validateDoi.value)")
        },onNoData: {
            if(triedAttempt > 3){
                self.isLogicDataFailed = true
                print("Tried 3 times still no data, give up...")
                return
            }else{
                print("No LOGIC Data, retry in 3 seconds, \(triedAttempt + 1) times")
                DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 3){
                    self.getLogic(triedAttempt: triedAttempt + 1)
                }
            }
        },onError: { error in
            self.isLogicDataFailed = true
        })
    }
    private func getAntifraud(triedAttempt: Int){
        // Get ANTIFRAUD
        self.getData(session: self.session, type: "ANTIFRAUD",onCompletion: {result in
            let antifraudObj = try? JSONDecoder().decode(AntifraudObj.self, from: result!)
            // Antifraud UI
            print("Antifraud Obj - Face Match: \(antifraudObj?.results.facesMatched?.count)")
            print("Antifraud Obj - Mark: \(antifraudObj?.results.mark?.prediction)")
            print("Antifraud Obj - Recapture: \(antifraudObj?.results.recapture?.prediction)")
            print("Antifraud Obj - Blacklist: \(antifraudObj?.results.blacklist)")
            print("Antifraud Obj - Abnormal Face: \(antifraudObj?.results.abnormalFace?.prediction)")
            print("Antifraud Obj - Abnormal Text: \(antifraudObj?.results.abnormalText?.prediction)")
            print("Antifraud Obj - Corner Cut: \(antifraudObj?.results.cornerCut?.prediction)")
            print("Antifraud Obj - Font Id: \(antifraudObj?.results.idFont?.prediction)")
            print(self.antifraudInfo)
        },onNoData: {
            if(triedAttempt > 3){
                self.isAntifraudDataFailed = true
                print("Tried 3 times still no data, give up...")
                return
            }else{
                print("No ANTIFRAUD Data, retry in 3 seconds, \(triedAttempt + 1) times")
                DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 3){
                    self.getAntifraud(triedAttempt: triedAttempt + 1)
                }
            }
        },onError: { error in
            self.isAntifraudDataFailed = true
        })
    }
    
    private func getVerify(triedAttempt: Int){
        // Get VERIFY
        self.getData(session: self.session, type: "VERIFY",onCompletion: {result in
            let verifyObj = try? JSONDecoder().decode(VerifyObj.self, from: result!)
            // Verify UI
            
            print("Verify Obj: \(verifyObj?.verified)")
        },onNoData: {
            if(triedAttempt > 3){
                self.isVerifyDataFailed = true
                print("Tried 3 times still no data, give up...")
                return
            }else{
                print("No VERIFY Data, retry in 3 seconds, \(triedAttempt + 1) times")
                DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 3){
                    self.getVerify(triedAttempt: triedAttempt + 1)
                }
            }
        },onError: { error in
            self.isVerifyDataFailed = true
        })
    }

 
    private func getEnrichData(){
        self.getSelfie(triedAttempt: 1)
        self.getLogic(triedAttempt: 1)
        self.getAntifraud(triedAttempt:1)
        self.getVerify(triedAttempt: 1)
    }
    
    private func loadLocalData(){
        let idCard = combineIDCard(frontCard: self.app.frontCard!, backCard: self.app.backCard!)
        dump(idCard)
        // Set UI
        DispatchQueue.main.async {
            self.ivFace.image = self.app.faceImage
            self.ivFace.layer.borderWidth = 2
            self.ivFace.layer.borderColor = UIColor.white.cgColor
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setToolbarHidden(true, animated: animated)
    }
    
    @IBAction func onDonePressed(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    init(session: String) {
        self.session = session
        super.init(nibName: String(describing: ResultViewController.self), bundle: Bundle.main)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
