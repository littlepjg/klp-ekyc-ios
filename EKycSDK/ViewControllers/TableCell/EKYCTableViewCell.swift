//
//  EKYCTableViewCell.swift
//  EKycSDK
//
//  Created by Gia Tu Nguyen on 30/09/2021.
//

import UIKit

class EKYCTableViewCell: UITableViewCell, UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var tableView: ContentSizedTableView!
    
    var list: [ElementAntifraudObj] = [ElementAntifraudObj]()
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let max_row = getHeighOfRow(input: list[indexPath.row].value, heightPerRow: 30)
//        print("Max row of \( list[indexPath.row].value): \(max_row)")
        if(max_row < getHeighOfRow(input: list[indexPath.row].name, heightPerRow: 30)) {
            return CGFloat(getHeighOfRow(input: list[indexPath.row].name, heightPerRow: 30))
        }else{
            return CGFloat(max_row)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        print("\(list.count)")
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AntifraudTableViewCell", for: indexPath) as! AntifraudTableViewCell
//        print("List: \(list[indexPath.row])")
        cell.labelName.text = list[indexPath.row].name
        cell.labelValue.text = list[indexPath.row].value
        cell.selectionStyle = .none
        return cell
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorColor = UIColor.init(red: 2, green: 253, blue: 174)
        tableView.register(UINib(nibName: "AntifraudTableViewCell", bundle: nil), forCellReuseIdentifier: "AntifraudTableViewCell")
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}
