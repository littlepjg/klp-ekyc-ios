//
//  AntifraudTableViewCell.swift
//  EKycSDK
//
//  Created by Gia Tu Nguyen on 30/09/2021.
//

import UIKit

class AntifraudTableViewCell: UITableViewCell {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}
