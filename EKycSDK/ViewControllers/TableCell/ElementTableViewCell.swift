//
//  ElementTableViewCell.swift
//  EKycSDK
//
//  Created by Gia Tu Nguyen on 30/09/2021.
//

import UIKit



class ElementTableViewCell: UITableViewCell,UITableViewDelegate,UITableViewDataSource {
    
    // Truyền list vào đây.
    var list: [ElementObj] = [ElementObj]()
    
    func RELOAD_DATA(){
        tableView.reloadData()
    }
    
    
    @IBOutlet weak var lblTitle: UILabel!
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let max_row = getHeighOfRow(input: list[indexPath.row].value, heightPerRow: 30)
//        print("Max row of \( list[indexPath.row].value): \(max_row)")
        if(max_row < getHeighOfRow(input: list[indexPath.row].name, heightPerRow: 30)) {
            return CGFloat(getHeighOfRow(input: list[indexPath.row].name, heightPerRow: 30))
        }else{
            return CGFloat(max_row)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        print("\(list.count)")
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ETableViewCell", for: indexPath) as! ETableViewCell
//        print("List: \(list[indexPath.row])")
        cell.lblTitle.text = list[indexPath.row].name
        cell.lblValue.text = list[indexPath.row].value
        cell.selectionStyle = .none
        return cell
    }
    
    
    @IBOutlet weak var tableView: ContentSizedTableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.sizeToFit()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorColor = UIColor.init(red: 2, green: 253, blue: 174)
        tableView.register(UINib(nibName: "ETableViewCell", bundle: nil), forCellReuseIdentifier: "ETableViewCell")
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
