//
//  AvatarCell.swift
//  EKycSDK
//
//  Created by Gia Tu Nguyen on 29/09/2021.
//

import UIKit


class AvatarCell: UITableViewCell {
    
    @IBOutlet weak var ivAvatar: CircularImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(false, animated: animated)
        // Configure the view for the selected state
    }

    
    
}
