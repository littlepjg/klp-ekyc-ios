//
//  OverviewViewController.swift
//  EKycSDK
//
//  Created by Gia Tu Nguyen on 29/09/2021.
//

import UIKit

class OverviewViewController: BaseViewController, UITableViewDelegate,UITableViewDataSource {
    var isConnected = false
    
    @IBOutlet weak var labelInfo: UILabel!
    var ocrInfo:[ElementObj] = [ElementObj]()
    var antifraudInfo:[ElementObj] = [ElementObj]()
    var logicInfo:[ElementObj] = [ElementObj]()
    var verifyInfo: [ElementObj] = [ElementObj]()
    let dispatchGroup = DispatchGroup()
    var avatar = UIImage(named: "logo_kalapa")
    private var isSetUp: Bool = false
    @IBOutlet weak var tableView: UITableView!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Return coresponding cell
        if(indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "AvatarCell", for: indexPath) as! AvatarCell
            //            cell.ivAvatar.image = self.app.faceImage
            cell.ivAvatar.image = avatar
            cell.ivAvatar.layer.borderWidth = 2
            cell.ivAvatar.layer.borderColor = UIColor.init(red: 2, green: 253, blue: 174).cgColor
            cell.selectionStyle = .none
            return cell
        }else if(indexPath.row == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "EKYCTableViewCell", for: indexPath) as! ElementTableViewCell
            cell.list = verifyInfo
            cell.lblTitle.text = "Kết quả eKYC"
            cell.selectionStyle = .none
            cell.isHidden = verifyInfo.isEmpty
            return cell
        }else if(indexPath.row == 2){
            let emptyCell = tableView.dequeueReusableCell(withIdentifier: "EmptyTableViewCell3", for: indexPath) as! EmptyTableViewCell
            emptyCell.backgroundColor = UIColor.red.withAlphaComponent(0)
            emptyCell.selectionStyle = .none
            return emptyCell
        }else if(indexPath.row == 3){
            let cell = tableView.dequeueReusableCell(withIdentifier: "ElementTableViewCell", for: indexPath) as! ElementTableViewCell
            cell.lblTitle.text = "Thông tin OCR"
            cell.list = ocrInfo
            cell.selectionStyle = .none
            cell.isHidden = ocrInfo.isEmpty
            return cell
        }else if (indexPath.row == 4){
            let antifraudCell = tableView.dequeueReusableCell(withIdentifier: "AntifraudTableViewCell", for: indexPath) as! ElementTableViewCell
//            print("Antifraud \(antifraudInfo)")
            antifraudCell.list = antifraudInfo
            antifraudCell.lblTitle.text = "Dấu hiệu bất thường"
            antifraudCell.selectionStyle = .none
            antifraudCell.isHidden = antifraudInfo.isEmpty
            return antifraudCell
        }else if (indexPath.row == 5){
            let emptyCell = tableView.dequeueReusableCell(withIdentifier: "EmptyTableViewCell1", for: indexPath) as! EmptyTableViewCell
            emptyCell.backgroundColor = UIColor.red.withAlphaComponent(0)
            emptyCell.selectionStyle = .none
            return emptyCell
        }
//        else if (indexPath.row == 6){
////            //            LogicTableViewCell
////            if( self.logicInfo.isEmpty || self.logicInfo.count == 0){
////                let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyTableViewCell3", for: indexPath) as! EmptyTableViewCell
////                return cell
////            }else{
//                let logicCell = tableView.dequeueReusableCell(withIdentifier: "LogicTableViewCell", for: indexPath) as! ElementTableViewCell
//                print("Antifraud \(logicInfo)")
//                logicCell.list = logicInfo
//                logicCell.lblTitle.text = "So khớp thông tin"
//                logicCell.selectionStyle = .none
//                return logicCell
////            }
//        }
        else{
            let emptyCell = tableView.dequeueReusableCell(withIdentifier: "EmptyTableViewCell2", for: indexPath) as! EmptyTableViewCell
            emptyCell.backgroundColor = UIColor.red.withAlphaComponent(0)
            emptyCell.selectionStyle = .none
            return emptyCell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 0){
            return 300
        }else if (indexPath.row == 1){
            let height = getHeightForView(list: verifyInfo, heightPerRow: 30) > 0  ? getHeightForView(list: verifyInfo, heightPerRow: 30) : 100
            return CGFloat(height + 40)
        }else if (indexPath.row == 2){
            return CGFloat(20)
        }else if (indexPath.row == 3){
            let height = getHeightForView(list: ocrInfo, heightPerRow: 30) > 0 ? getHeightForView(list: ocrInfo, heightPerRow: 30) : 100
            return CGFloat(height + 40)
        }else if (indexPath.row == 4){
            let height = getHeightForView(list: antifraudInfo, heightPerRow: 30) > 0 ? getHeightForView(list: antifraudInfo, heightPerRow: 30) : 100
            return CGFloat(height  + 40)
        }else if (indexPath.row == 5){
            return CGFloat(10)
        }
        else if (indexPath.row == 6){
            let height = getHeightForView(list: logicInfo, heightPerRow: 30) > 0 ? getHeightForView(list: logicInfo, heightPerRow: 30) : 100
            return CGFloat(height  + 20)
        }
        else{
            return CGFloat(0)
        }
    }
    
    @IBAction func onDonePressed(_ sender: Any) {
        self.app.session = nil
        self.app.frontCard = nil
        self.app.backCard = nil
        self.app.faceImage = nil
        self.app.frontImage = nil
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func onBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getLocalData()
        self.getEnrichData()
        dispatchGroup.notify(queue: .main){
            print("Now we go")
            self.labelInfo.isHidden = true
            self.setupView()
        }
    }
    
    private func getLocalData(){
        if(self.app.faceImage != nil){
            avatar = self.app.faceImage
        }
        if(self.app.frontCard != nil && self.app.backCard != nil ) {
            let idCard = combineIDCard(frontCard: self.app.frontCard!, backCard: self.app.backCard!)
            
            if(idCard.data != nil && idCard.data!.fields != nil &&
                idCard.data!.fields?.id_number != nil && !idCard.data!.fields!.id_number.isEmpty) { ocrInfo.append(ElementObj(name: "ID", value:idCard.data!.fields!.id_number))}
            if(idCard.data != nil && idCard.data!.fields != nil &&
                idCard.data!.fields?.name != nil && !idCard.data!.fields!.name.isEmpty) { ocrInfo.append(ElementObj(name: "Họ và tên", value:idCard.data!.fields!.name))}
            if(idCard.data != nil && idCard.data!.fields != nil &&
                idCard.data!.fields?.birthday != nil && !idCard.data!.fields!.birthday.isEmpty) { ocrInfo.append(ElementObj(name: "Ngày sinh", value:idCard.data!.fields!.birthday))}
            if(idCard.data != nil && idCard.data!.fields != nil &&
                idCard.data!.fields?.gender != nil && !idCard.data!.fields!.gender.isEmpty) { ocrInfo.append(ElementObj(name: "Giới tính", value:idCard.data!.fields!.gender))}
            if(idCard.data != nil && idCard.data!.fields != nil &&
                idCard.data!.fields?.ethnicity != nil && !idCard.data!.fields!.ethnicity.isEmpty) { ocrInfo.append(ElementObj(name: "Dân tộc", value:idCard.data!.fields!.ethnicity))}
            if(idCard.data != nil && idCard.data!.fields != nil &&
                idCard.data!.fields?.home != nil && !idCard.data!.fields!.home.isEmpty) { ocrInfo.append(ElementObj(name: "Nguyên quán", value:idCard.data!.fields!.home))}
            if(idCard.data != nil && idCard.data!.fields != nil &&
                idCard.data!.fields?.resident != nil && !idCard.data!.fields!.resident.isEmpty) { ocrInfo.append(ElementObj(name: "Thường trú", value:idCard.data!.fields!.resident))}
            if(idCard.data != nil && idCard.data!.fields != nil &&
                idCard.data!.fields?.doi != nil && !idCard.data!.fields!.doi.isEmpty) { ocrInfo.append(ElementObj(name: "Ngày cấp", value:idCard.data!.fields!.doi))}
            if(idCard.data != nil && idCard.data!.fields != nil &&
                idCard.data!.fields?.poi != nil && !idCard.data!.fields!.poi.isEmpty) { ocrInfo.append(ElementObj(name: "Nơi cấp", value:idCard.data!.fields!.poi))}
            if(idCard.data != nil && idCard.data!.fields != nil &&
                idCard.data!.fields?.features != nil && !idCard.data!.fields!.features.isEmpty) { ocrInfo.append(ElementObj(name: "Đặc điểm nhân dạng", value:idCard.data!.fields!.features))}
            if(idCard.data != nil && idCard.data!.fields != nil &&
                idCard.data!.fields?.national != nil && !idCard.data!.fields!.national.isEmpty) { ocrInfo.append(ElementObj(name: "Quốc gia", value:idCard.data!.fields!.national))}
            if(idCard.data != nil && idCard.data!.fields != nil &&
                idCard.data!.fields?.religion != nil && !idCard.data!.fields!.religion.isEmpty) { ocrInfo.append(ElementObj(name: "Tôn giáo", value:idCard.data!.fields!.religion))}
            if(idCard.data != nil && idCard.data!.fields != nil &&
                idCard.data!.fields?.country != nil && !idCard.data!.fields!.country.isEmpty) { ocrInfo.append(ElementObj(name: "Quốc Tịch", value:idCard.data!.fields!.country))}
            if(idCard.data != nil && idCard.data!.fields != nil &&
                idCard.data!.fields?.doe != nil && !idCard.data!.fields!.doe.isEmpty) { ocrInfo.append(ElementObj(name: "Ngày hết hạn", value:idCard.data!.fields!.doe))}
            if(idCard.data != nil && idCard.data!.fields != nil &&
                idCard.data!.fields?.type != nil && !idCard.data!.fields!.type.isEmpty) { ocrInfo.append(ElementObj(name: "Loại giấy tờ", value:idCard.data!.fields!.type))}
        }
        
    }
    
    
    private func getEnrichData(){
        dispatchGroup.enter()
        self.getSelfie(triedAttempt: 1)
        dispatchGroup.enter()
//        self.getLogic(triedAttempt: 0)
//        dispatchGroup.enter()
        self.getAntifraud(triedAttempt:3)
        dispatchGroup.enter()
        self.getVerify(triedAttempt: 1)
    }
    
    // Start of Enrich
    private func getSelfie(triedAttempt: Int){
        // Get SELFIE
        self.getData(session: app.session!, type: "SELFIE",onCompletion: {result in
            let selfieobj = try? JSONDecoder().decode(SelfieObj.self, from: result!)
            // SELFIE UI
            print("Selfie Data: \(selfieobj?.data.isMatched) \(selfieobj?.data.matchingScore) \(selfieobj?.error.message)")
            self.verifyInfo.append(ElementObj(name: "Xác thực sự sống", value: "✅"))
            self.verifyInfo.append(ElementObj(name: "So khớp khuôn mặt", value: selfieobj?.data.isMatched ?? false  ? "✅"  : "❌" ))
            self.verifyInfo.append(ElementObj(name: "Độ tương đồng", value: "\(selfieobj!.data.matchingScore)%"))
            self.dispatchGroup.leave()
        },onNoData: {
            if(triedAttempt > 3){
                print("Tried 3 times still no data, give up...")
                self.dispatchGroup.leave()
                return
            }else{
                print("No SELFIE Data, retry in 3 seconds, \(triedAttempt + 1) times")
                DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 3){
                    self.getSelfie(triedAttempt: triedAttempt + 1)
                }
            }
        },onError: { error in
            self.dispatchGroup.leave()
        })
    }
    
    
    private func getLogic(triedAttempt: Int){
        self.dispatchGroup.leave()
        // Get LOGIC
        self.getData(session: app.session!, type: "LOGIC",onCompletion: {result in
            let logicObj = try? JSONDecoder().decode(LogicObj.self, from: result!)
            // LOGIC UI
            
            if logicObj?.data.validateAddress.value != nil {self.logicInfo.append(ElementObj(name: logicObj!.data.validateAddress.message.vi, value: logicObj!.data.validateAddress.value! ?  "❌":"✅"))}
            if logicObj?.data.validateGender.value != nil {self.logicInfo.append(ElementObj(name: logicObj!.data.validateGender.message.vi, value: logicObj!.data.validateGender.value! ?  "❌":"✅"))}
            if logicObj?.data.validateYob.value != nil {self.logicInfo.append(ElementObj(name: logicObj!.data.validateYob.message.vi, value: logicObj!.data.validateYob.value! ?  "❌":"✅"))}
            if logicObj?.data.validateProvince.value != nil {self.logicInfo.append(ElementObj(name: logicObj!.data.validateProvince.message.vi, value: logicObj!.data.validateProvince.value! ?  "❌":"✅"))}
            if logicObj?.data.validateRegisterAge.value != nil {self.logicInfo.append(ElementObj(name: logicObj!.data.validateRegisterAge.message.vi, value: logicObj!.data.validateRegisterAge.value! ?  "❌":"✅"))}
            if logicObj?.data.validateExpiry.value != nil {self.logicInfo.append(ElementObj(name: logicObj!.data.validateExpiry.message.vi, value: logicObj!.data.validateExpiry.value! ?  "❌":"✅"))}
            if logicObj?.data.validateDob.value != nil {self.logicInfo.append(ElementObj(name: logicObj!.data.validateDob.message.vi, value: logicObj!.data.validateDob.value! ?  "❌":"✅"))}
            if logicObj?.data.validateDoi.value != nil {self.logicInfo.append(ElementObj(name: logicObj!.data.validateDoi.message.vi, value: logicObj!.data.validateDoi.value! ?  "❌":"✅"))}
            self.dispatchGroup.leave()
        },onNoData: {
            if(triedAttempt > 3){
                print("Tried 3 times still no data, give up...")
                self.dispatchGroup.leave()
                return
            }else{
                print("No LOGIC Data, retry in 3 seconds, \(triedAttempt + 1) times")
                DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 3){
                    self.getLogic(triedAttempt: triedAttempt + 1)
                }
            }
        },onError: { error in
            self.dispatchGroup.leave()
        })
    }
    
    private func getAntifraud(triedAttempt: Int){
        // Get ANTIFRAUD
        self.getData(session: app.session!, type: "ANTIFRAUD",onCompletion: {result in
            let antifraudObj = try? JSONDecoder().decode(AntifraudObj.self, from: result!)
            // Antifraud UI
            print("Antifraud Obj Parsed")
            print(antifraudObj)
            dump(result)
            self.antifraudInfo.append(ElementObj(name: "Khuôn mặt trùng", value: antifraudObj?.results.facesMatched?.count != nil ?
                                                    ((antifraudObj?.results.facesMatched?.count)! > 0 ? "❌" : "✅") : "❔"))
            
            self.antifraudInfo.append(ElementObj(name: "Dấu nổi bất thường", value: antifraudObj?.results.mark?.prediction != nil ?
                                                    ((antifraudObj?.results.mark?.prediction)! ? "❌" : "✅") : "❔"))
            
            self.antifraudInfo.append(ElementObj(name: "Chụp lại màn hình", value: antifraudObj?.results.recapture?.prediction != nil ?
                                                    ((antifraudObj?.results.recapture?.prediction)! ? "❌" : "✅") : "❔"))
            
            self.antifraudInfo.append(ElementObj(name: "Danh sách đen", value: antifraudObj?.results.blacklist != nil ?
                                                    ((antifraudObj?.results.blacklist)! ? "❌" : "✅") : "❔"))
            
            self.antifraudInfo.append(ElementObj(name: "Mặt bất thường", value: (antifraudObj?.results.abnormalFace?.prediction) != nil ?
                                                    ((antifraudObj?.results.abnormalFace?.prediction)! ? "❌" : "✅") : "❔"))
            
            self.antifraudInfo.append(ElementObj(name: "Font chữ bất thường", value: (antifraudObj?.results.abnormalText?.prediction) != nil ?
                                                    ((antifraudObj?.results.abnormalText?.prediction)!  ? "❌" : "✅") : "❔"))
            
            self.antifraudInfo.append(ElementObj(name: "Ảnh bị cắt góc", value: antifraudObj?.results.cornerCut?.prediction != nil ?
                                                    ((antifraudObj?.results.cornerCut!.prediction)! ? "❌" : "✅") : "❔"))
            
            self.antifraudInfo.append(ElementObj(name: "Id bất thường", value: antifraudObj?.results.idFont?.prediction != nil ?
                                                    ((antifraudObj?.results.idFont?.prediction)! ? "❌" : "✅") : "❔"))
            
            
            self.dispatchGroup.leave()
        },onNoData: {
            if(triedAttempt > 3){
                print("Tried 3 times still no data, give up...")
                self.dispatchGroup.leave()
                return
            }else{
                print("No ANTIFRAUD Data, retry in 3 seconds, \(triedAttempt + 1) times")
                DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 3){
                    self.getAntifraud(triedAttempt: triedAttempt + 1)
                }
            }
        },onError: { error in
            self.dispatchGroup.leave()
        })
    }
    
    private func getVerify(triedAttempt: Int){
        // Get VERIFY
        self.getData(session: app.session!, type: "VERIFY",onCompletion: {result in
            let verifyObj = try? JSONDecoder().decode(VerifyObj.self, from: result!)
            // Verify UI
            print("Verify Obj: \(String(describing: verifyObj))")
            self.verifyInfo.append(ElementObj(name: "Giấy tờ hợp lệ", value: verifyObj?.verified != nil ?  verifyObj!.verified ? "✅"  : "❌" : "❔" ))
            self.dispatchGroup.leave()
        },onNoData: {
            if(triedAttempt > 3){
                print("Tried 3 times still no data, give up...")
                self.dispatchGroup.leave()
                return
            }else{
                print("No VERIFY Data, retry in 3 seconds, \(triedAttempt + 1) times")
                DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 3){
                    self.getVerify(triedAttempt: triedAttempt + 1)
                }
            }
        },onError: { error in
            self.dispatchGroup.leave()
        })
    }
    
    
    // End of Enrich
    private func setupView(){
        tableView.register(UINib(nibName: "AvatarCell", bundle: nil), forCellReuseIdentifier: "AvatarCell")
        tableView.register(UINib(nibName: "ElementTableViewCell", bundle: nil), forCellReuseIdentifier: "EKYCTableViewCell")
        tableView.register(EmptyTableViewCell.self, forCellReuseIdentifier: "EmptyTableViewCell3")
        tableView.register(UINib(nibName: "ElementTableViewCell", bundle: nil), forCellReuseIdentifier: "ElementTableViewCell")
        tableView.register(UINib(nibName: "ElementTableViewCell", bundle: nil), forCellReuseIdentifier: "AntifraudTableViewCell")
        tableView.register(EmptyTableViewCell.self, forCellReuseIdentifier: "EmptyTableViewCell1")
        tableView.register(UINib(nibName: "ElementTableViewCell", bundle: nil), forCellReuseIdentifier: "LogicTableViewCell")
        tableView.register(EmptyTableViewCell.self, forCellReuseIdentifier: "EmptyTableViewCell2")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }

}
