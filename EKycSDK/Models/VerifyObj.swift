//
//  VerifyObj.swift
//  EKycSDK
//
//  Created by Gia Tu Nguyen on 28/09/2021.
//
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let verifyObj = try? newJSONDecoder().decode(VerifyObj.self, from: jsonData)

import Foundation

// MARK: - VerifyObj
struct VerifyObj: Codable {
    let idCardInfo: IDCardInfo
    let careerName, careerDob, careerAddr, careerGender: String?
    let verified: Bool
}

// MARK: - IDCardInfo
struct IDCardInfo: Codable {
    let id, name: String
}
 
