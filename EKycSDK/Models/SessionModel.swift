//
//  SessionModel.swift
//  EKycSDK
//
//  Created by Gia Tu Nguyen on 22/09/2021.
//

import Foundation

class SessionModel: Decodable {
    let token: String
    let username: String
    let roles: [String]
 
}
