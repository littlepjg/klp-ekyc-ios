//
//  MessageManager.swift
//  EKycSDK
//
//  Created by Gia Tu Nguyen on 26/09/2021.
//

import Foundation

final class LivenessMessage: Codable{
    let status: Int
    let message: String
    let base64_image: String?
}
