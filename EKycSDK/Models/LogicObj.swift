//
//  LogicObj.swift
//  EKycSDK
//
//  Created by Gia Tu Nguyen on 28/09/2021.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let logicObj = try? newJSONDecoder().decode(LogicObj.self, from: jsonData)

import Foundation

// MARK: - LogicObj
struct LogicObj: Codable {
    let message: String
    let data: LogicData

    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case data
    }
}

// MARK: - DataClass
struct LogicData: Codable {
    let validateProvince, validateGender, validateYob, validateAddress, validateRegisterAge, validateExpiry, validateDob, validateDoi: LogicElement

    enum CodingKeys: String, CodingKey {
        case validateProvince = "validate_province"
        case validateGender = "validate_gender"
        case validateYob = "validate_yob"
        case validateAddress = "validate_address"
        case validateRegisterAge = "validate_register_age"
        case validateExpiry = "validate_expiry"
        case validateDob = "validate_dob"
        case validateDoi = "validate_doi"
    }
}

struct LogicElement: Codable{
    let message: LogicMessage
    let value: Bool?
}

// MARK: - Message
struct LogicMessage: Codable {
    let vi, en: String
}
