//
//  IDCard.swift
//  EKycSDK
//
//  Created by Gia Tu Nguyen on 27/09/2021.
//

import Foundation

class IDCard: NSObject,Decodable {
    var data: MyData?
    var error: ResponseError
}

class MyData: Decodable{
    var fields: Fields?
    var card_type: String
}

class Fields: Decodable{
    var birthday: String
    var country: String
    var doe: String
    var doi: String
    var ethnicity: String
    var features: String
    var gender: String
    var home: String
    var home_entities: HomeEntities?
    var id_number: String
    var name: String
    var national: String
    var poi: String
    var religion: String
    var resident: String
    var resident_entities: ResidentEntities?
    var type: String
}

class ResponseError: Decodable{
    var code: Int
    var message: String
}

class ResidentEntities: Decodable{
    var district: String?
    var province: String?
    var unknown: String?
    var ward: String?
}

class HomeEntities: Decodable{
    var district: String?
    var province: String?
}
