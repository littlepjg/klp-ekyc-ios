//
//  OfferResponse.swift
//  EKycSDK
//
//  Created by Gia Tu Nguyen on 24/09/2021.
//

import Foundation

class OfferResponse: Decodable{
    let sdp: String
    let type: String
}
