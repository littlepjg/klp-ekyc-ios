//
//  SelfieObj.swift
//  EKycSDK
//
//  Created by Gia Tu Nguyen on 28/09/2021.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let selfieObj = try? newJSONDecoder().decode(SelfieObj.self, from: jsonData)

import Foundation

// MARK: - SelfieObj
struct SelfieObj: Codable {
    let data: SelfieData
    let error: SelfieError
}

// MARK: - DataClass
struct SelfieData: Codable {
    let allFaceBBS: [[Int]]
    let anomalyScore: Double
    let eulerAngles: [Double]
    let faceBbox: [Int]
    let faceKeypoints: [[Int]]
    let isMatched: Bool
    let matchingScore, rotatingAngle: Int
    let spamScore: Double

    enum CodingKeys: String, CodingKey {
        case allFaceBBS = "all_face_bbs"
        case anomalyScore = "anomaly_score"
        case eulerAngles = "euler_angles"
        case faceBbox = "face_bbox"
        case faceKeypoints = "face_keypoints"
        case isMatched = "is_matched"
        case matchingScore = "matching_score"
        case rotatingAngle = "rotating_angle"
        case spamScore = "spam_score"
    }
}

// MARK: - Error
struct SelfieError: Codable {
    let code: Int
    let message: String
}
