//
//  ElementAntifraudObj.swift
//  EKycSDK
//
//  Created by Gia Tu Nguyen on 30/09/2021.
//

import Foundation

class ElementAntifraudObj{
    let name: String
    let value: String
    init(name: String, value : String) {
        self.name = name.unfoldSubSequences(limitedTo: 32).joined(separator: "\n")
        self.value = value
        print(self.name)
        print(self.value)
    }
    
}
